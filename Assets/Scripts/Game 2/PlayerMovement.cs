﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour

{
    // Start is called before the first frame update
    private Rigidbody _rigidbody;
    [SerializeField] private float forwardForce;
    [SerializeField] private float _forwardSpeed;
    [SerializeField] private float _evasionSpeed;
    [SerializeField] private Vector2 northWest;
    [SerializeField] private Vector2 northEast;
    [SerializeField] private Vector2 southWest;
    [SerializeField] private Vector2 southEast;
    [SerializeField] private Vector2 center;

    private Vector3 newPos;
    private Countdown countdown;
    [SerializeField] private Tracker tracker;
    [SerializeField] private MeshRenderer brod;

    private bool kalibracijaGotova = false;
    private bool kalibracijaNijePocela = true;
    private Vector2 srednjeOdstupanje;
    private Vector2 prosjecnaSredina;
    private List<Vector2> mjerenja = new List<Vector2>();
    private Vector2 ocitanje;
    private float xRazlika;
    private float yRazlika;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        countdown = GetComponent<Countdown>();
    }

    // Update is called once per frame
    void Update()
    {
        if (kalibracijaNijePocela & tracker.faceTracked())
        {
            StartCoroutine(Kalibracija());
        }

        if (kalibracijaGotova)
        {
            Pomicanje();
        }
    }

    IEnumerator Kalibracija()
    {
        kalibracijaNijePocela = false;
        countdown.enabled = true;
        int i;
        int brojOcitanja = 0;
        Vector2 zbroj = new Vector2(0, 0);
        float xOdstupanje = 0f;
        float yOdstupanje = 0f;

        for (i = 0; i <= 49; i++)
        {
            if (tracker.faceTracked())
            {
                brojOcitanja++;
                ocitanje = tracker.getGazeDirection();
                mjerenja.Add(ocitanje);
                zbroj += ocitanje;
                yield return new WaitForSeconds(0.06f);
            }
        }

        prosjecnaSredina = new Vector2(zbroj.x / brojOcitanja, zbroj.y / brojOcitanja);

        foreach (Vector2 mjerenje in mjerenja)
        {
            xOdstupanje += (mjerenje.x - prosjecnaSredina.x) * (mjerenje.x - prosjecnaSredina.x);
            yOdstupanje += (mjerenje.y - prosjecnaSredina.y) * (mjerenje.y - prosjecnaSredina.y);
        }
        srednjeOdstupanje = new Vector2(xOdstupanje / brojOcitanja * (brojOcitanja - 1), yOdstupanje / brojOcitanja * (brojOcitanja - 1));

        kalibracijaGotova = true;
        brod.enabled = true;
    }

    void Pomicanje()
    {
        if (tracker.faceTracked())
        {
            ocitanje = tracker.getGazeDirection();
        }
        xRazlika = ocitanje.x - prosjecnaSredina.x;
        yRazlika = ocitanje.y - prosjecnaSredina.y;

        if (System.Math.Abs(xRazlika) > 2 * srednjeOdstupanje.x & System.Math.Abs(yRazlika) > 2 * srednjeOdstupanje.y)
        {
            if (xRazlika > 0 & yRazlika > 0)
            {
                newPos.Set(southEast.x, southEast.y, 0);
            }

            else if (xRazlika < 0 & yRazlika < 0)
            {
                newPos.Set(northWest.x, northWest.y, 0);
            }

            else if (xRazlika > 0 & yRazlika < 0)
            {
                newPos.Set(northEast.x, northEast.y, 0);
            }

            else if (xRazlika < 0 & yRazlika > 0)
            {
                newPos.Set(southWest.x, southWest.y, 0);
            }
        }
        newPos.z = transform.position.z - Time.deltaTime * _forwardSpeed;
        //transform.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * _evasionSpeed);
        _rigidbody.MovePosition(Vector3.Lerp(transform.position, newPos, Time.deltaTime * _evasionSpeed));
        //_rigidbody.position = Vector3.Lerp(transform.position, newPos, Time.deltaTime * _evasionSpeed);
    }
}
