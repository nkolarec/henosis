﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform player;
    public float offset;
    private Vector3 newPos;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        newPos.Set(transform.position.x, transform.position.y, player.position.z + offset);
        transform.position = newPos;
    }
}
