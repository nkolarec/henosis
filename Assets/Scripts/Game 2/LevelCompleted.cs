﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelCompleted : MonoBehaviour
{
    private bool _levelNotOver = true;
    [SerializeField] private FollowPlayer _followScript;
    [SerializeField] private Canvas _successCanvas;
    [SerializeField] private Tracker _tracker;
    [SerializeField] private AudioSource _successAudio;
    [SerializeField] private GameObject _starScore;
    [SerializeField] private GameObject[] stars;
    [SerializeField] private GameObject[] outlines;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z < -2050 && _levelNotOver)
        {
            _levelNotOver = false;
            EndLevel();
        }
    }

    void EndLevel()
    {
        _followScript.enabled = false;
        _tracker.enabled = false;
        _starScore.SetActive(false);
        _successCanvas.gameObject.SetActive(true);
        _successAudio.Play();
        StartCoroutine(waitThenStar());
    }

    IEnumerator waitThenStar()
    {
        int numStars = PlayerPrefs.GetInt("_final_score");
        yield return new WaitForSecondsRealtime(1f);
        for (int i = 0; i < numStars; i++)
        {
            stars[i].SetActive(true);
            outlines[i].SetActive(false);
            yield return new WaitForSecondsRealtime(0.4f);
        }
    }
}
