﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Countdown : MonoBehaviour
{
    [SerializeField] private Text countdown;
    [SerializeField] private Text instruction;
    [SerializeField] private Image redDot;

    // Start is called before the first frame update
    void Start()
    {
        countdown.enabled = true;
        StartCoroutine(Odbrojavanje());
    }

    IEnumerator Odbrojavanje()
    {
        int i;
        for(i = 3; i >= 0; i--)
        {
            countdown.text = i.ToString();
            yield return new WaitForSeconds(1f);
        }
        countdown.enabled = false;
        instruction.enabled = false;
        redDot.enabled = false;
        enabled = false;
    }
}
