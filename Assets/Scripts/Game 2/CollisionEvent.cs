﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CollisionEvent : MonoBehaviour
{
    [SerializeField] private Transform player;
    [SerializeField] private GameObject explosionEffect;

    private PlayerMovement movement;
    private Rigidbody rb;
    private int _score;
    [SerializeField] private Text score;
    private Vector3 placement;
    private GameObject star;
    private ParticleSystem starEffect;
    [SerializeField] private Tracker tracker;
    [SerializeField] private FollowPlayer followScript;
    [SerializeField] private Canvas _endCanvas;

    // Start is called before the first frame update
    public void OnCollisionEnter(Collision col)
    {
        if (col.collider.tag == "Asteroid")
        {
            movement.enabled = false;
            Explode();
            tracker.enabled = false;
            followScript.enabled = false;
            _endCanvas.gameObject.SetActive(true);
            _score = PlayerPrefs.GetInt("score");
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Star")
        {
            star = other.gameObject;
            starEffect = star.GetComponentInChildren<ParticleSystem>();
            starEffect.Play();
            star.transform.DetachChildren();
            Destroy(star);

            _score++;
            PlayerPrefs.SetInt("_final_score", _score);
            score.text = (_score).ToString();
        }
    }

    // Update is called once per frame
    void Start()
    {
        _score = PlayerPrefs.GetInt("score");
        PlayerPrefs.SetInt("_final_score", _score);
        score.text = (_score).ToString();
        rb = GetComponent<Rigidbody>();
        movement = GetComponent<PlayerMovement>();
    }

    void Explode()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);
        
    }
}
