﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGeneration : MonoBehaviour
{
    private static Vector2 center = new Vector2(0.0f, -1.3f);
    private static Vector2 northWest = new Vector2(12.0f, 5.3f);
    private static Vector2 northEast = new Vector2(-12.0f, 5.3f);
    private static Vector2 southWest = new Vector2(12.0f, -5.3f);
    private static Vector2 southEast = new Vector2(-12.0f, -5.3f);
    private static Vector2[] positionArray = new Vector2[4] { northEast, northWest, southEast, southWest };

    public static float zAxis = -200;

    [SerializeField] private GameObject asteroid1Prefab;
    [SerializeField] private GameObject asteroid2Prefab;
    [SerializeField] private GameObject asteroid3Prefab;
    [SerializeField] private GameObject starPrefab;
    [SerializeField] private int _numAsteroidClusters;
    [SerializeField] private int _numStars;

    private GameObject[] asteroidArray;
    private HashSet<int> _starPositions;

    // Start is called before the first frame update
    void Start()
    {
        if(_numStars > _numAsteroidClusters)
        {
            Debug.LogError("Number of stars must NOT be greater than number of clusters");
        }

        asteroidArray = new GameObject[3] { asteroid1Prefab, asteroid2Prefab, asteroid3Prefab };
        _starPositions = new HashSet<int>();
        while (_starPositions.Count < _numStars)
        {
            int randomPos = (int)Random.Range(1f, (float)_numAsteroidClusters);
            _starPositions.Add(randomPos);
        }
        StartCoroutine(asteroidSpawner());
    }

    private void SpawnAsteroid(float zAsxis, bool spawnStar)
    {
        IList<int> usedPos = new List<int>();
        int astNum = Random.Range(2, 4);

        for (int i = 0; i < astNum; i++)
        {

            int ast = Random.Range(0, 3);
            int posit = Random.Range(0, 4);

            while (usedPos.Contains(posit))
            {
                posit = Random.Range(0, 4);
            }
            usedPos.Add(posit);

            Vector3 pos = new Vector3(positionArray[posit].x, positionArray[posit].y, zAxis);

            GameObject spawnedAsteroid = Instantiate(asteroidArray[ast]) as GameObject;
            spawnedAsteroid.transform.position = pos;

        }

        Vector3 pos2 = new Vector3(center.x, center.y, zAxis);
        GameObject spawnedAsteroid2 = Instantiate(asteroid1Prefab) as GameObject;
        spawnedAsteroid2.transform.position = pos2;

        int posit1 = Random.Range(0, 4);
        while (usedPos.Contains(posit1))
        {
            posit1 = Random.Range(0, 4);
        }

        Vector3 pos1 = new Vector3(positionArray[posit1].x, positionArray[posit1].y, zAxis);

        if (spawnStar)
        {
            GameObject spawnedStar = Instantiate(starPrefab) as GameObject;
            spawnedStar.transform.position = pos1;
        }
    }

    IEnumerator asteroidSpawner()
    {
        for (int i = 1; i < _numAsteroidClusters+1; i++)
        {
            zAxis = i * -200;
            SpawnAsteroid(zAxis, _starPositions.Contains(i));
            yield return new WaitForSeconds(1f);
        }
    }



}
