﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarCollect : MonoBehaviour
{
    private ParticleSystem collectEffect;

    private Vector3 placement;

    public void CollectStar(GameObject star)
    {
        placement = star.transform.position;
        collectEffect = star.GetComponentInChildren<ParticleSystem>();
        collectEffect.transform.position = placement;
        collectEffect.Play();
        star.transform.DetachChildren();
        Destroy(star);
    }
}
