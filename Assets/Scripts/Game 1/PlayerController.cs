﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private Tracker _tracker;
    [SerializeField] private Collider _lava;
    [SerializeField] private GameManager _gameManager;

    private Animator _anim;
    private CharacterController _controller;

    [SerializeField] private float forwardSpeed = 7.0f;
    [SerializeField] private float backSpeed = 3.0f;
    [SerializeField] private float turnSpeed = 150.0f;
    [SerializeField] private float gravity = 10.0f;

    private int _movingPlatformLayerMask;
    private bool _onPlatform = false;


    private void Awake()
    {
        _movingPlatformLayerMask = LayerMask.GetMask("MovingPlatform");
    }
    private void Start()
    {
        _controller = GetComponent<CharacterController>();
        _anim = gameObject.GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        if (!_controller.enabled)
            return;

        Vector3 moveDirection = Vector3.down * gravity;
        float verticalInput;
        float turn;
        if (_tracker.faceTracked())
        {
            verticalInput = _tracker.getEyeClosure().magnitude;
            turn = _tracker.getHeadRotation()[1];
        }
        else
        {
            verticalInput = Input.GetAxis("Vertical");
            turn = Input.GetAxis("Horizontal");
        }

        if (_controller.isGrounded)
        {
            moveDirection += transform.forward * verticalInput * (verticalInput > 0 ? forwardSpeed : backSpeed);
        }

        float forwardMovement = Vector3.Dot(_controller.velocity, transform.forward);

        if (forwardMovement > 0)
        {
            _anim.SetInteger("AnimationPar", 1);
        }
        else if (forwardMovement < 0)
        {
            _anim.SetInteger("AnimationPar", 2);
        }
        else
        {
            _anim.SetInteger("AnimationPar", 0);
        }

        transform.Rotate(0, turn * turnSpeed * Time.deltaTime, 0);
        _controller.Move(moveDirection * Time.deltaTime);
    }

    private void FixedUpdate()
    {
        if (!_onPlatform)
        {
            return;
        }
        RaycastHit rayHit;
        if (Physics.Raycast(new Ray(transform.position, -transform.up), out rayHit))
        {
            //platform vise nije pod igracem
            if (rayHit.transform != transform.parent)
            {
                transform.SetParent(null, true);
                _onPlatform = false;
            }
        }
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.collider == _lava)
        {
            _controller.enabled = false;
            _anim.SetInteger("AnimationPar", 0);
            _gameManager.Die();
        }
        else if (!_onPlatform && 1 << hit.gameObject.layer == _movingPlatformLayerMask)
        {
            //igrac stao na platformu
            RaycastHit rayHit;
            if (Physics.Raycast(new Ray(transform.position, -transform.up), out rayHit, Mathf.Infinity, _movingPlatformLayerMask))
            {
                transform.SetParent(rayHit.transform, true);
                _onPlatform = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Star"))
        {
            _gameManager.increaseStarScore();
            FindObjectOfType<StarCollect>().CollectStar(other.gameObject);
        }
        else if (other.CompareTag("Oilcan"))
        {
            _gameManager.increaseCanScore();
            Destroy(other.gameObject);
        }
        else if (other.Equals(_gameManager.endGameCollider))
        {
            _gameManager.tryToStartRocket();
        }
    }

}