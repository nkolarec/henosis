﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] private Transform[] places;
    [SerializeField] private int numobj;
    [SerializeField] private GameObject obj;
    private ArrayList Taken = new ArrayList();
    private int j;
    
    void Start()
    {
        j = UnityEngine.Random.Range(0, places.Length);
        for (int i = 0; i < numobj; i++)
        {
            while (Taken.Contains(j))
            {
                j = UnityEngine.Random.Range(0, places.Length);

            }

            Instantiate(obj, places[j].position, places[j].rotation);
            Taken.Add(j);
        }
        

    }

   
}
