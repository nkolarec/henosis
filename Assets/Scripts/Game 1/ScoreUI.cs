﻿using UnityEngine.UI;
using UnityEngine;

public class ScoreUI : MonoBehaviour
{
    [SerializeField] private int _maxScore = 5;
    [SerializeField] private bool _showMaxScore;
    public int MaxScore
    {
        get
        {
            return _maxScore;
        }
    }

    private int _score;
    public int Score
    {
        get { return _score; }
        set
        {
            if (value > MaxScore)
            {
                return;
            }
            _score = value;
            if (_showMaxScore)
            {
                scoreText.text = $"{_score}/{_maxScore}";
            }
            else
            {
                scoreText.text = $"{_score}";
            }
        }
    }

    [SerializeField] private Text scoreText;
}
