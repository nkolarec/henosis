﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformController : MonoBehaviour
{
    [SerializeField] private Transform _origin;
    [SerializeField] private Transform _destination;
    [SerializeField] private float _travelTime;
    [Tooltip("How long to wait when the destination is reached")]
    [SerializeField] private float _waitTime;
    public bool moving = false;

    private Transform _org;
    private Transform _dest;
    private bool _travelToDestination = true;
    private float _elapsedTime = 0;
    void Start()
    {
        transform.position = _origin.position;
        _org = _origin;
        _dest = _destination;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!moving)
        {
            return;
        }
        transform.position = Vector3.Lerp(_org.position, _dest.position, _elapsedTime / _travelTime);

        if (_elapsedTime >= _travelTime)
        {
            _travelToDestination = !_travelToDestination;
            _elapsedTime = 0;
            Transform temp = _org;
            _org = _dest;
            _dest = temp;
            StartCoroutine(StopPlatformForWaitTime());
        }

        _elapsedTime += Time.fixedDeltaTime;
    }

    private IEnumerator StopPlatformForWaitTime()
    {
        moving = false;
        yield return new WaitForSeconds(_waitTime);
        moving = true;
    }
}
