﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour
{

    [SerializeField] private Canvas endCanvas;
    [SerializeField] private Collider _lava;
    [SerializeField] private GameObject _astronaut;
    [SerializeField] private ScoreUI _canScore;
    [SerializeField] private ScoreUI _starScore;
    [SerializeField] private Collider _endGameCollider;
    public Collider endGameCollider
    {
        get
        {
            return _endGameCollider;
        }
    }
    [SerializeField] private float _sinkDepth = 3;
    [SerializeField] private float _sinkTime = 3;
    [SerializeField] private float _sinkInterval = 0.1f;

    private bool gameOver = false;

    public bool GameOver
    {
        private set
        {
            gameOver = value;
        }
        get
        {
            return gameOver;
        }
    }

    public void Die()
    {
        GameOver = true;
        endCanvas.gameObject.SetActive(true);
        StartCoroutine(Sink());
        Camera.main.transform.SetParent(null);
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void tryToStartRocket()
    {
        if (_canScore.Score >= _canScore.MaxScore)
        {
            PlayerPrefs.SetInt("score", _starScore.Score);
            SceneManager.LoadScene("Cutscene");
        }
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene("Start");
    }

    IEnumerator Sink()
    {
        float time = _sinkTime;

        while (time > 0)
        {
            yield return new WaitForSeconds(_sinkInterval);
            _astronaut.transform.position += Vector3.down * _sinkDepth / (_sinkTime / _sinkInterval);
            time -= _sinkInterval;
        }
        //Restart();
    }

    public void increaseCanScore()
    {
        _canScore.Score++;
    }

    public void increaseStarScore()
    {
        _starScore.Score++;
    }
}
