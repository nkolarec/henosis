using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.InteropServices;

#if PLATFORM_ANDROID && UNITY_2018_3_OR_NEWER
using UnityEngine.Android;
#endif

using System.IO;
using System.Linq;

/** Class that implements the behavior of tracking application.
 * 
 * This is the core class that shows how to use visage|SDK capabilities in Unity. It connects with visage|SDK through calls to 
 * native methods that are implemented in VisageTrackerUnityPlugin.
 * It uses tracking data to transform objects that are attached to it in ControllableObjects list.
 */
public class Tracker : MonoBehaviour
{
    #region Properties

    [Header("Tracker configuration settings")]
    //Tracker configuration file name.
    public string ConfigFileEditor;
    public string ConfigFileStandalone;
    public string ConfigFileIOS;
    public string ConfigFileAndroid;
    public string ConfigFileOSX;
    public string ConfigFileWebGL;


    [Header("Tracking settings")]
#if UNITY_WEBGL
    public const int MAX_FACES = 1;
#else
    public const int MAX_FACES = 4;
#endif

    private bool trackerInited = false;

    private bool isTracking = false;
    public int[] TrackerStatus = new int[MAX_FACES];

    [Header("Camera settings")]
    public Material CameraViewMaterial;
    public float CameraFocus;
    public int Orientation = 0;
    private int currentOrientation = 0;
    public int isMirrored = 1;
    private int currentMirrored = 1;
    public int camDeviceId = 0;
    private int AndroidCamDeviceId = 0;
    private int currentCamDeviceId = 0;
    public int defaultCameraWidth = -1;
    public int defaultCameraHeight = -1;
    private bool doSetupMainCamera = true;
    private bool camInited = false;

    [Header("Texture settings")]
    public int ImageWidth = 800;
    public int ImageHeight = 600;
    public int TexWidth = 512;
    public int TexHeight = 512;
    private Texture2D texture = null;
    private Color32[] texturePixels;
    private GCHandle texturePixelsHandle;
#if UNITY_ANDROID
	private TextureFormat TexFormat = TextureFormat.RGB24;
#else
    private TextureFormat TexFormat = TextureFormat.RGBA32;
#endif

    [HideInInspector]
    public bool frameForAnalysis = false;
    public bool frameForRecog = false;
    private bool texCoordsStaticLoaded = false;

#if UNITY_ANDROID
	private AndroidJavaObject androidCameraActivity;
	private bool AppStarted = false;
	AndroidJavaClass unity;
#endif

    private float[] _headRotation = new float[3];
    private float[] _gazeDirection = new float[2];
    private float[] _globalGazeDirection = new float[3];
    private float[] _eyeClosure = new float[2];

    #endregion

    #region Native code printing

    private bool enableNativePrinting = true;

    //For printing from native code
    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate void MyDelegate(string str);

    //Function that will be called from the native wrapper
    static void CallBackFunction(string str)
    {
        Debug.Log("::CallBack : " + str);
    }

    #endregion

    private void Awake()
    {
#if PLATFORM_ANDROID && UNITY_2018_3_OR_NEWER
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            Permission.RequestUserPermission(Permission.Camera);
#endif

#if UNITY_WEBGL
        VisageTrackerNative._preloadFile(Application.streamingAssetsPath + "/Visage Tracker/" + ConfigFileWebGL);
        VisageTrackerNative._preloadFile(Application.streamingAssetsPath + "/Visage Tracker/" + LicenseString.licenseString);
        VisageTrackerNative._preloadAnalysisData("visageAnalysisData.js");
#endif

        // Set callback for printing from native code
        if (enableNativePrinting)
        {
            /*  MyDelegate callback_delegate = new MyDelegate(CallBackFunction);
                // Convert callback_delegate into a function pointer that can be
                // used in unmanaged code.
                IntPtr intptr_delegate = Marshal.GetFunctionPointerForDelegate(callback_delegate);
                // Call the API passing along the function pointer.
                VisageTrackerNative.SetDebugFunction(intptr_delegate);*/
        }


#if UNITY_ANDROID
		Unzip();
#endif

        string licenseFilePath = Application.streamingAssetsPath + "/" + "/Visage Tracker/";

        // Set license path depending on platform
        switch (Application.platform)
        {

            case RuntimePlatform.IPhonePlayer:
                licenseFilePath = "Data/Raw/Visage Tracker/";
                break;
            case RuntimePlatform.Android:
                licenseFilePath = Application.persistentDataPath + "/";
                break;
            case RuntimePlatform.OSXPlayer:
                licenseFilePath = Application.dataPath + "/Resources/Data/StreamingAssets/Visage Tracker/";
                break;
            case RuntimePlatform.OSXEditor:
                licenseFilePath = Application.dataPath + "/StreamingAssets/Visage Tracker/";
                break;
            case RuntimePlatform.WebGLPlayer:
                licenseFilePath = "";
                break;
            case RuntimePlatform.WindowsEditor:
                licenseFilePath = Application.streamingAssetsPath + "/Visage Tracker/";
                break;
        }

        bool licFileExists = false;

        //checking if license key file exists in StreamingAssets/Visage Tracker
#if UNITY_IPHONE
        FileInfo info = new FileInfo(Path.Combine(licenseFilePath, LicenseString.licenseString));
        if (info.Exists == false)
        licFileExists = true;
#else
        if (File.Exists(licenseFilePath + LicenseString.licenseString))
            licFileExists = true;
#endif

        if (licFileExists)
        {
#if UNITY_STANDALONE_WIN
            VisageTrackerNative._initializeLicense(licenseFilePath);
#else
            VisageTrackerNative._initializeLicense(licenseFilePath + LicenseString.licenseString);
#endif
        }
        else //case when license key is embedded
            VisageTrackerNative._initializeLicense(LicenseString.licenseString);

    }


    private void Start()
    {
        // Set configuration file path and name depending on a platform
        string configFilePath = Application.streamingAssetsPath + "/" + ConfigFileStandalone;

        switch (Application.platform)
        {
            case RuntimePlatform.IPhonePlayer:
                configFilePath = "Data/Raw/Visage Tracker/" + ConfigFileIOS;
                break;
            case RuntimePlatform.Android:
                configFilePath = Application.persistentDataPath + "/" + ConfigFileAndroid;
                break;
            case RuntimePlatform.OSXPlayer:
                configFilePath = Application.dataPath + "/Resources/Data/StreamingAssets/Visage Tracker/" + ConfigFileOSX;
                break;
            case RuntimePlatform.OSXEditor:
                configFilePath = Application.dataPath + "/StreamingAssets/Visage Tracker/" + ConfigFileOSX;
                break;
            case RuntimePlatform.WebGLPlayer:
                configFilePath = ConfigFileWebGL;
                break;
            case RuntimePlatform.WindowsEditor:
                configFilePath = Application.streamingAssetsPath + "/" + ConfigFileEditor;
                break;
        }

        // Initialize tracker with configuration and MAX_FACES
        trackerInited = InitializeTracker(configFilePath);

        // Get current device orientation
        Orientation = GetDeviceOrientation();

        // Open camera in native code
        camInited = OpenCamera(Orientation, camDeviceId, defaultCameraWidth, defaultCameraHeight, isMirrored);

        if (SystemInfo.graphicsDeviceType == UnityEngine.Rendering.GraphicsDeviceType.OpenGLCore)
            Debug.Log("Notice: if graphics API is set to OpenGLCore, the texture might not get properly updated.");
    }

    
    private void Update()
    {
        //signals analysis and recognition to stop if camera or tracker are not initialized and until new frame and tracking data are obtained
        frameForAnalysis = false;
        frameForRecog = false;
        
        if (!isTrackerReady())
            return;

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

#if (UNITY_IPHONE || UNITY_ANDROID) && UNITY_EDITOR
		// tracking will not work if the target is set to Android or iOS while in editor
		return;
#endif

        if (isTracking)
        {
#if UNITY_ANDROID
			if (VisageTrackerNative._frameChanged())
			{
				texture = null;
				doSetupMainCamera = true;
			}	
#endif
            Orientation = GetDeviceOrientation();

            // Check if orientation or camera device changed
            if (currentOrientation != Orientation || currentCamDeviceId != camDeviceId || currentMirrored != isMirrored)
            {
                currentCamDeviceId = camDeviceId;
                currentOrientation = Orientation;
                currentMirrored = isMirrored;

                // Reopen camera with new parameters 
                OpenCamera(currentOrientation, currentCamDeviceId, defaultCameraWidth, defaultCameraHeight, currentMirrored);
                doSetupMainCamera = true;
            }

            // grab current frame and start face tracking
            VisageTrackerNative._grabFrame();

            VisageTrackerNative._track();
            VisageTrackerNative._getTrackerStatus(TrackerStatus);

            //After the track has been preformed on the new frame, the flags for the analysis and recognition are set to true
            frameForAnalysis = true;
            frameForRecog = true;

            // Set main camera field of view based on camera information
            if (doSetupMainCamera)
            {
                // Get camera information from native
                VisageTrackerNative._getCameraInfo(out CameraFocus, out ImageWidth, out ImageHeight);
                doSetupMainCamera = false;
            }

            //face at index 0 is tracked
            if (TrackerStatus[0] == (int)TrackStatus.OK)
            {
                VisageTrackerNative._getHeadRotation(_headRotation, 0);
                VisageTrackerNative._getGazeDirection(_gazeDirection, 0);
                VisageTrackerNative._getGazeDirectionGlobal(_globalGazeDirection, 0);
                VisageTrackerNative._getEyeClosure(_eyeClosure, 0);
            }
        }

        RefreshImage();
    }

    #region Getters
    /// <summary>
    /// Returns the estimated rotation of the head, in radians. Rotation is expressed with three values determining the rotations around the three axes x, y and z, in radians. This means that the values represent the pitch, yaw and roll of the head, respectively. The zero rotation (values 0, 0, 0) corresponds to the face looking straight ahead along the camera axis. Positive values for pitch correspond to head turning down. Positive values for yaw correspond to head turning right in the input image. Positive values for roll correspond to head rolling to the left in the input image. The values are in radians.
    /// </summary>
    /// <returns>head rotation</returns>
    public Vector3 getHeadRotation()
    {
        return new Vector3(_headRotation[0], _headRotation[1], _headRotation[2]);
    }
    /// <summary>
    /// Direction is expressed with two values x and y, in radians. Values (0, 0) correspond to person looking straight. X is the horizontal rotation with positive values corresponding to person looking to his/her left. Y is the vertical rotation with positive values corresponding to person looking down.
    /// </summary>
    /// <returns>gaze direction</returns>
    public Vector2 getGazeDirection()
    {
        return new Vector2(_gazeDirection[0], _gazeDirection[1]);
    }
    /// <summary>
    /// Global gaze direction is the current estimated gaze direction relative to the camera axis. Direction is expressed with three values determining the rotations around the three axes x, y and z, i.e. pitch, yaw and roll.
    /// </summary>
    /// <returns>global gaze direction</returns>
    public Vector3 getGlobalGazeDirection()
    {
        return new Vector3(_globalGazeDirection[0], _globalGazeDirection[1], _globalGazeDirection[2]);
    }
    /// <summary>
    /// Discrete eye closure value is expressed with two values: index 0 represents closure of left eye. Index 1 represents closure of right eye. Value of 1 represents open eye. Value of 0 represents closed eye.
    /// </summary>
    /// <returns>eye closure</returns>
    public Vector2 getEyeClosure()
    {
        return new Vector2(_eyeClosure[0], _eyeClosure[1]);
    }
    public bool faceTracked()
    {
        return TrackerStatus[0] == (int)TrackStatus.OK;
    }

    #endregion

    public bool isTrackerReady()
    {
        if (camInited && trackerInited)
        {
            isTracking = true;
        }
        else
        {
            isTracking = false;
        }
        return isTracking;
    }

    void OnDestroy()
    {
#if UNITY_ANDROID
		this.androidCameraActivity.Call("closeCamera");
#else
        camInited = !(VisageTrackerNative._closeCamera());
#endif
    }

    public void onButtonSwitch()
    {
#if UNITY_WEBGL
            camInited = false;
            VisageTrackerNative._openCamera(ImageWidth, ImageHeight, isMirrored, "OnSuccessCallbackCamera", "OnErrorCallbackCamera");
#else
        camDeviceId = (camDeviceId == 1) ? 0 : 1;
#endif
    }


    /// <summary>
    /// Initialize tracker with maximum number of faces - MAX_FACES.
    /// Additionally, depending on a platform set an appropriate shader.
    /// </summary>
    /// <param name="config">Tracker configuration path and name.</param>
    private bool InitializeTracker(string config)
    {
        Debug.Log("Visage Tracker: Initializing tracker with config: '" + config + "'");

#if (UNITY_IPHONE || UNITY_ANDROID) && UNITY_EDITOR
		return false;
#endif

#if UNITY_WEBGL
        // initialize tracker
        VisageTrackerNative._initTracker(config, MAX_FACES, "CallbackInitTracker");
        return trackerInited;
#else
        VisageTrackerNative._initTracker(config, MAX_FACES);
        return true;
#endif
    }

    #region Callback Function for WEBGL

    public void CallbackInitTracker()
    {
        Debug.Log("TrackerInited");
        trackerInited = true;
    }

    public void OnSuccessCallbackCamera()
    {
        Debug.Log("CameraSuccess");
        camInited = true;
    }

    public void OnErrorCallbackCamera()
    {
        Debug.Log("CameraError");
    }

    #endregion

    /// <summary>
    /// Update Unity texture with frame data from native camera.
    /// </summary>
    private void RefreshImage()
    {
        // Initialize texture
        if (texture == null && isTracking && ImageWidth > 0)
        {
            TexWidth = Convert.ToInt32(Math.Pow(2.0, Math.Ceiling(Math.Log(ImageWidth) / Math.Log(2.0))));
            TexHeight = Convert.ToInt32(Math.Pow(2.0, Math.Ceiling(Math.Log(ImageHeight) / Math.Log(2.0))));
            texture = new Texture2D(TexWidth, TexHeight, TexFormat, false);

            var cols = texture.GetPixels32();
            for (var i = 0; i < cols.Length; i++)
                cols[i] = Color.black;

            texture.SetPixels32(cols);
            texture.Apply(false);

            CameraViewMaterial.SetTexture("_BaseMap", texture);

#if UNITY_STANDALONE_WIN
            // "pin" the pixel array in memory, so we can pass direct pointer to it's data to the plugin,
            // without costly marshaling of array of structures.
            texturePixels = ((Texture2D)texture).GetPixels32(0);
            texturePixelsHandle = GCHandle.Alloc(texturePixels, GCHandleType.Pinned);
#endif
        }

        if (texture != null && isTracking && TrackerStatus[0] != (int)TrackStatus.OFF)
        {
#if UNITY_STANDALONE_WIN
            // send memory address of textures' pixel data to VisageTrackerUnityPlugin
            VisageTrackerNative._setFrameData(texturePixelsHandle.AddrOfPinnedObject());
            ((Texture2D)texture).SetPixels32(texturePixels, 0);
            ((Texture2D)texture).Apply();
#elif UNITY_IPHONE || UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX || UNITY_ANDROID
			if (SystemInfo.graphicsDeviceVersion.StartsWith ("Metal"))
				VisageTrackerNative._bindTextureMetal (texture.GetNativeTexturePtr ());
			else
				VisageTrackerNative._bindTexture ((int)texture.GetNativeTexturePtr ());
#elif UNITY_WEBGL
            VisageTrackerNative._bindTexture(texture.GetNativeTexturePtr());
#endif
        }
    }


    /// <summary>
    /// Get current device orientation.
    /// </summary>
    /// <returns>Returns an integer:
    /// <list type="bullet">
    /// <item><term>0 : DeviceOrientation.Portrait</term></item>
    /// <item><term>1 : DeviceOrientation.LandscapeRight</term></item>
    /// <item><term>2 : DeviceOrientation.PortraitUpsideDown</term></item>
    /// <item><term>3 : DeviceOrientation.LandscapeLeft</term></item>
    /// </list>
    /// </returns>
    private int GetDeviceOrientation()
    {
        int devOrientation;

#if UNITY_ANDROID
		//Device orientation is obtained in AndroidCameraPlugin so we only need information about whether orientation is changed
		int oldWidth = ImageWidth;
		int oldHeight = ImageHeight;

		VisageTrackerNative._getCameraInfo(out CameraFocus, out ImageWidth, out ImageHeight);

		if ((oldWidth!=ImageWidth || oldHeight!=ImageHeight) && ImageWidth != 0 && ImageHeight !=0 && oldWidth != 0 && oldHeight !=0 )
			devOrientation = (Orientation ==1) ? 0:1;
		else
			devOrientation = Orientation;
#else
        if (Input.deviceOrientation == DeviceOrientation.Portrait)
            devOrientation = 0;
        else if (Input.deviceOrientation == DeviceOrientation.PortraitUpsideDown)
            devOrientation = 2;
        else if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
            devOrientation = 3;
        else if (Input.deviceOrientation == DeviceOrientation.LandscapeRight)
            devOrientation = 1;
        else if (Input.deviceOrientation == DeviceOrientation.FaceUp)
            devOrientation = Orientation;
        else if (Input.deviceOrientation == DeviceOrientation.Unknown)
            devOrientation = Orientation;
        else
            devOrientation = 0;
#endif

        return devOrientation;
    }


    /// <summary> 
    /// Open camera from native code. 
    /// </summary>
    /// <param name="orientation">Current device orientation:
    /// <list type="bullet">
    /// <item><term>0 : DeviceOrientation.Portrait</term></item>
    /// <item><term>1 : DeviceOrientation.LandscapeRight</term></item>
    /// <item><term>2 : DeviceOrientation.PortraitUpsideDown</term></item>
    /// <item><term>3 : DeviceOrientation.LandscapeLeft</term></item>
    /// </list>
    /// </param>
    /// <param name="camDeviceId">ID of the camera device.</param>
    /// <param name="width">Desired width in pixels (pass -1 for default 800).</param>
    /// <param name="height">Desired width in pixels (pass -1 for default 600).</param>
    /// <param name="isMirrored">true if frame is to be mirrored, false otherwise.</param>
    private bool OpenCamera(int orientation, int cameraDeviceId, int width, int height, int isMirrored)
    {
#if UNITY_ANDROID
		if (cameraDeviceId == AndroidCamDeviceId && AppStarted)
			return false;

        AndroidCamDeviceId = cameraDeviceId;
		//camera needs to be opened on main thread
		this.androidCameraActivity.Call("runOnUiThread", new AndroidJavaRunnable(() => {
			this.androidCameraActivity.Call("closeCamera");
			this.androidCameraActivity.Call("GrabFromCamera", width, height, camDeviceId);
		}));
		AppStarted = true;
		return true;
#elif UNITY_WEBGL
        VisageTrackerNative._openCamera(ImageWidth, ImageHeight, isMirrored, "OnSuccessCallbackCamera", "OnErrorCallbackCamera");
        return false;
#else
        VisageTrackerNative._openCamera(orientation, cameraDeviceId, width, height, isMirrored);
        return true;
#endif
    }

#if UNITY_ANDROID
	void Unzip()
	{
		string[] pathsNeeded = {
			"candide3.fdp",
			"candide3.wfm",
			"jk_300.fdp",
			"jk_300.wfm",
			"Head Tracker.cfg",
			"Facial Features Tracker - High.cfg",
			"Face Detector.cfg",
			"visage_powered.png",
			"warning.png",
			"bdtsdata/FF/ff.dat",
			"bdtsdata/LBF/lv",
			"bdtsdata/LBF/vfadata/ad/ad0.lbf",
			"bdtsdata/LBF/vfadata/ad/ad1.lbf",
			"bdtsdata/LBF/vfadata/ad/ad2.lbf",
			"bdtsdata/LBF/vfadata/ad/ad3.lbf",
			"bdtsdata/LBF/vfadata/ad/ad4.lbf",
			"bdtsdata/LBF/vfadata/ad/ae.bin",
			"bdtsdata/LBF/vfadata/ad/regressor.lbf",
			"bdtsdata/LBF/vfadata/ed/ed0.lbf",
			"bdtsdata/LBF/vfadata/ed/ed1.lbf",
			"bdtsdata/LBF/vfadata/ed/ed2.lbf",
			"bdtsdata/LBF/vfadata/ed/ed3.lbf",
			"bdtsdata/LBF/vfadata/ed/ed4.lbf",
			"bdtsdata/LBF/vfadata/ed/ed5.lbf",
			"bdtsdata/LBF/vfadata/ed/ed6.lbf",
			"bdtsdata/LBF/vfadata/gd/gd.lbf",
			"bdtsdata/NN/fa.lbf",
			"bdtsdata/NN/fc.lbf",
			"bdtsdata/NN/fr.bin",
			"bdtsdata/NN/pr.bin",
			"bdtsdata/NN/is.bin",
			"bdtsdata/NN/model.bin",
			"bdtsdata/LBF/ye/lp11.bdf",
			"bdtsdata/LBF/ye/W",
			"bdtsdata/LBF/ye/landmarks.txt"
			, "dev.vlc"
		};
		string outputDir;
		string localDataFolder = "Visage Tracker";

		outputDir = Application.persistentDataPath;

		if (!Directory.Exists(outputDir))
		{
			Directory.CreateDirectory(outputDir);
		}
		foreach (string filename in pathsNeeded)
		{
			WWW unpacker = new WWW("jar:file://" + Application.dataPath + "!/assets/" + localDataFolder + "/" + filename);

			while (!unpacker.isDone) { }

			if (!string.IsNullOrEmpty(unpacker.error))
			{
				continue;
			}

			if (filename.Contains("/"))
			{
				string[] split = filename.Split('/');
				string name = "";
				string folder = "";
				string curDir = outputDir;

				for (int i = 0; i < split.Length; i++)
				{
					if (i == split.Length - 1)
					{
						name = split[i];
					}
					else
					{
						folder = split[i];
						curDir = curDir + "/" + folder;
					}
				}
				if (!Directory.Exists(curDir))
				{
					Directory.CreateDirectory(curDir);
				}

				File.WriteAllBytes("/" + curDir + "/" + name, unpacker.bytes);
			}
			else
			{
				File.WriteAllBytes("/" + outputDir + "/" + filename, unpacker.bytes);
			}
		}
	}
#endif
}
